<?php
/**
 * @var WP_Post $review
 */

?>

<input type="url"
       id="rcno-reviews-custom-book-cover-src"
       class="widefat"
       name="rcno_reviews_custom_book_cover_src"
       value="<?php echo esc_url( get_post_meta( $review->ID, 'rcno_reviews_custom_book_cover_src', true ) ) ?>"/>

