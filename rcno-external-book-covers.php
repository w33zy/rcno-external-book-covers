<?php
/**
 * Plugin Name:     Recencio External Book Covers
 * Plugin URI:      https://wzymedia.com
 * Description:     An example extension for using external URLS for book covers in the Recencio Book Reviews plugin
 * Author:          w33zy
 * Author URI:      https://wzymedia.com
 * Text Domain:     wzy-media
 * Domain Path:     /languages
 * Version:         1.2.0
 *
 * @package         wzy_Media
 */

defined( 'ABSPATH' ) || exit;

register_activation_hook( __FILE__, static function() {
		include_once ABSPATH . 'wp-admin/includes/plugin.php';

		if ( ! is_plugin_active( 'recencio-book-reviews/recencio-book-reviews.php' ) ) {
			wp_die( 'Please make sure the Recencio Book Review plugin is installed and activated.', 'wzy-media' );
		}
	}
);

if ( ! class_exists( 'Rcno_External_Book_Covers' ) ) {

	require_once WP_PLUGIN_DIR . '/recencio-book-reviews/includes/abstracts/Abstract_Rcno_Extension.php';

	class Rcno_External_Book_Covers extends Abstract_Rcno_Extension {

		public function __construct() {
			$this->id       = 'rcno_external_book_covers';
			$this->image    = plugin_dir_url( __FILE__ ) . 'assets/images/R.png';
			$this->title    = __( 'External Book Covers', 'wzy-media' );
			$this->desc     = __( 'Adds the option to use an external URL to use as the book cover.', 'wzy-media' );
			$this->settings = false;
		}

		/**
		 * All methods that we want to be called by the Rcno_Reviews_Extensions class goes here.
		 */
		public function load() {
			$this->add_filters();
			$this->add_actions();
		}

		/**
		 * Add WordPress filters are called here.
		 */
		private function add_filters() {
			add_filter( 'rcno_all_book_meta_keys', array( $this, 'add_to_meta_keys' ) );
			add_filter( 'rcno_book_cover_url', array( $this, 'book_cover_url' ), 10, 2 );

			// We are returning false as we don't want our custom URL to be converted to a 'href' tag.
			add_filter( 'rcno_skip_url_conversion', '__return_false' );
		}

		/**
		 * Add WordPress actions are called here.
		 */
		private function add_actions() {
			add_action( 'do_meta_boxes', array( $this, 'rcno_book_cover_url_metabox' ), 10 );
			add_action( 'rcno_save_admin_book_cover_metadata', array( $this, 'save_admin_book_cover_metadata' ) );
		}

		/**
		 * Register the metabox for the book cover URL.
		 *
		 * @return void
		 */
		public function rcno_book_cover_url_metabox() {
			// Add editor metabox for the book cover.
			add_meta_box(
				'rcno_book_cover_url_metabox',
				__( 'Custom Book Cover URL', 'rcno-reviews' ),
				array( $this, 'do_rcno_book_cover_url_metabox' ),
				'rcno_review',
				'side',
				'high'
			);
		}

		/**
		 * Render the metabox for the book cover URL.
		 *
		 * @param WP_Post $review
		 *
		 * @return void
		 */
		public function do_rcno_book_cover_url_metabox( $review ) {
			include __DIR__ . '/includes/rcno-book-cover-url-metabox.php';
		 }

		/**
		 * Save the custom book cover URL in the database.
		 *
		 * @return void
		 */
		public function save_admin_book_cover_metadata() {
			$index = 'rcno_reviews_custom_book_cover_src';

			if ( ! empty( $_POST[ $index ] ) ) {
				update_post_meta( (int) get_the_ID(), $index, esc_url( $_POST[ $index ] ) );
			}
		}

		/**
		 * Add our custom meta key to the list of meta keys
		 *
		 * @param array $meta_keys The list of meta keys.
		 *
		 * @return array
		 */
		public function add_to_meta_keys( $meta_keys ) {
			$meta_keys['rcno_reviews_custom_book_cover_src'] = 'Custom Book Cover URL';

			return $meta_keys;
		}

		/**
		 * Change the book cover URL to the custom URL.
		 *
		 * @param string $url The URL of the book cover.
		 * @param int    $review_id The ID of the review we are attaching to.
		 *
		 * @return string
		 */
		public function book_cover_url( $url, $review_id ) {
			$template = new Rcno_Template_Tags( 'rcno_reviews', '1.17.0' );
			$custom_url = $template->get_the_rcno_book_meta( $review_id, 'rcno_reviews_custom_book_cover_src', '', false );

			if ( empty( $custom_url ) ) {
				return $url;
			}

			return $custom_url;
		}

	}
}

/**
 * Register the extension with the main Recencio plugin by adding its info
 * via the 'rcno_reviews_extensions' filter.
 *
 * This is how we tell the Recencio plugin about our extension
 * We are using a anonymous function here to avoid function name clashes with other extensions.
 *
 * @param  array $extensions
 *
 * @return array
 */
add_filter( 'rcno_reviews_extensions', function ( $extensions ) {
	$extensions['rcno_external_book_covers'] = 'Rcno_External_Book_Covers';

	return $extensions;
} );
